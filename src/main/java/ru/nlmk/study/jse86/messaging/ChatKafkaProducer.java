package ru.nlmk.study.jse86.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import ru.nlmk.study.jse86.model.Message;
import ru.nlmk.study.jse86.repository.MessageRepository;

@Slf4j
@Component
public class ChatKafkaProducer {

    private final KafkaTemplate<String, Message> kafkaTemplate;

    private final MessageRepository messageRepository;

    @Value("${message.topic}")
    private String chatTopic;

    @Autowired
    public ChatKafkaProducer(KafkaTemplate<String, Message> kafkaTemplate, MessageRepository messageRepository) {
        this.kafkaTemplate = kafkaTemplate;
        this.messageRepository = messageRepository;
    }

    public void sendMessage(Message message) {
        log.info("sent message through kafka with text: {}", message.getText());
        kafkaTemplate.send(chatTopic, message);
        messageRepository.save(message);
    }
}
