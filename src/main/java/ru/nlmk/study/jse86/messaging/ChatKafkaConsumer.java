package ru.nlmk.study.jse86.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ru.nlmk.study.jse86.model.Message;

@Slf4j
@Component
public class ChatKafkaConsumer {

    @Value("${message.topic}")
    private String chatTopic;

    @Autowired
    private final SimpMessagingTemplate template;

    public ChatKafkaConsumer(SimpMessagingTemplate template) {
        this.template = template;
    }

    @KafkaListener(topics = "${message.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumeMessage(Message message) {
        log.info("consumed data from topic: {} with text: {}", chatTopic, message.getText());

        template.convertAndSend("/topic/public", message);
    }
}
