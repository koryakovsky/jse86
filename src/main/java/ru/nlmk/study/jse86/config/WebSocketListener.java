package ru.nlmk.study.jse86.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import ru.nlmk.study.jse86.model.Message;
import ru.nlmk.study.jse86.model.MessageType;

@Slf4j
@Component
public class WebSocketListener {

    private final SimpMessageSendingOperations sendingOperations;

    @Autowired
    public WebSocketListener(SimpMessageSendingOperations sendingOperations) {
        this.sendingOperations = sendingOperations;
    }

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectEvent event) {
        log.info("New user connected to websocket");
    }

    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        String username = (String) accessor.getSessionAttributes().get("username");
        if (username != null) {
            log.info("User {} disconnected from the chat", username);
            Message message = new Message();
            message.setMessageType(MessageType.LEAVE);
            message.setSender(username);

            sendingOperations.convertAndSend("/topic/public", message);
        }
    }
}
