package ru.nlmk.study.jse86;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jse86Application {

	public static void main(String[] args) {
		SpringApplication.run(Jse86Application.class, args);
	}

}
