package ru.nlmk.study.jse86.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.study.jse86.messaging.ChatKafkaProducer;
import ru.nlmk.study.jse86.model.Message;

@Slf4j
@RestController
@RequestMapping(value = "/kafka/chat")
public class ChatController {

    private final ChatKafkaProducer producer;

    @Autowired
    public ChatController(ChatKafkaProducer producer) {
        this.producer = producer;
    }

    @GetMapping(value = "/sendMessage")
    @MessageMapping("/sendMessage")
    public void sendMessage(Message message) {
        producer.sendMessage(message);
    }

    @SendTo("/topic/public")
    @MessageMapping("/chat.addUser")
    public Message addUser(@Payload Message message, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", message.getSender());
        return message;
    }
}
