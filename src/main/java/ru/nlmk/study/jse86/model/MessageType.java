package ru.nlmk.study.jse86.model;

public enum MessageType {
    CHAT,
    JOIN,
    LEAVE
}
