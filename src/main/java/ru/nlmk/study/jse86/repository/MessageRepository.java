package ru.nlmk.study.jse86.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nlmk.study.jse86.model.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
}
